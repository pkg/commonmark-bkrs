Source: commonmark-bkrs
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 12),
               dh-python,
Build-Depends-Indep: help2man,
                     python3-all,
                     python3-docutils,
                     python3-setuptools
Standards-Version: 4.5.0
Homepage: https://github.com/rolandshoemaker/CommonMark-py
Vcs-Git: https://salsa.debian.org/python-team/packages/commonmark-bkrs.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/commonmark-bkrs

Package: python3-commonmark-bkrs
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Suggests: python-commonmark-bkrs-doc (= ${source:Version})
Replaces: python-commonmark-bkrs (<< 0.5.4+ds-3)
Breaks: python-commonmark-bkrs (<< 0.5.4+ds-3)
Description: Python parser for the CommonMark Markdown spec -- Python 3
 Pure Python port of `jgm''s CommonMark, a Markdown parser and renderer for the
 CommonMark (http://commonmark.org) specification, using only native modules.
 .
 This provides the CommonMark-py Python package as developed by Bibek Kafle and
 Roland Shoemaker (BKRS). After release 0.5.4, their project moved to `Read the
 Docs' (http://readthedocs.org) and major changes broke compatibility. However
 some Python packages may still depend on their last implementation, hence this
 Debian package which is meant to smooth the transition.
 .
 This package installs the library for Python 3.

Package: python-commonmark-bkrs-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: www-browser
Enhances: python-commonmark-bkrs (= ${source:Version}),
          python3-commonmark-bkrs (= ${source:Version})
Description: Python parser for the CommonMark Markdown spec -- doc
 Pure Python port of `jgm''s CommonMark, a Markdown parser and renderer for the
 CommonMark (http://commonmark.org) specification, using only native modules.
 .
 This provides the CommonMark-py Python package as developed by Bibek Kafle and
 Roland Shoemaker (BKRS). After release 0.5.4, their project moved to `Read the
 Docs' (http://readthedocs.org) and major changes broke compatibility. However
 some Python packages may still depend on their last implementation, hence this
 Debian package which is meant to smooth the transition.
 .
 This is the common documentation package.
